﻿
(function (seedExamples, $, undefined) {

    var self = this;

    seedExamples.init = function () {
        self.$resultsNumber = $('#numberOfRowResults');
        self.$results = $('#rowResults');

        $resultsNumber.on('change', function () {
            seedExamples.refreshRowResults();
        })
    };

    seedExamples.refreshRowResults = function (evt) {
        $results.load("/Seed/GetResultRows/" + $resultsNumber.val());
    }

})(window.seedExamples = window.seedExamples || {}, jQuery);


