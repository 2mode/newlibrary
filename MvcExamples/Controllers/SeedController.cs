﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcExamples.Controllers
{
    public class SeedController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetResultRows(int? rows)
        {
            return PartialView("_Result2", rows);
        }

        public ActionResult SeedTests()
        {
            return View();
        }

        public ActionResult SeedExamples()
        {
            return View();
        }
    }
}