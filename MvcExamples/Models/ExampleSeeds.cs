﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcExamples.Models
{
    public class Example
    {
        private List<Item> properties;
        
        public int ExampleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal NetWorth { get; set; }
        public decimal? Investments { get; set; }
        public string Email { get; set; }
        public DateTime Created { get; set; }
        public List<Item> Properties {
            get { return properties ?? new List<Item>(); }
            set { properties = value; }
        }
    }
    public class Item
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public DateTime Created { get; set; }
    }

    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public DateTime? Created { get; set; }
    }
}