﻿using System;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using System.Globalization;

namespace NewLibrary.ForString
{
    public static class StringExtensions
    {
        public static HtmlString toHtmlString(this string str)
        {
            return new HtmlString(str);
        }

        public static bool isNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static string isBlank(this string s)
        {
            return s.isNullOrEmpty() ? "" : s;
        }

        public static string isBlank(this string s, string replacement)
        {
            return s.isNullOrEmpty() ? replacement : s;
        }

        public static string removeStart(this string input, string start)
        {
            string s = input.isBlank();
            string t = start.isBlank();
            return (s.StartsWith(t)) ? s.Remove(0, t.Length) : s;
        }

        public static string removeEnd(this string input, string end)
        {
            string s = input.isBlank();
            string e = end.isBlank();
            return (s.EndsWith(e)) ? s.Remove(s.Length - e.Length, e.Length) : s;
        }

        public static string removeStartEnd(this string input, string start, string end)
        {
            string s = input.removeStart(start);
            string t = s.removeEnd(end);
            return t;
        }

        public static string numbersOnly(this string input, string additionalChars)
        {
            var output = new StringBuilder("");
            for (int i = 0; i < input.isBlank().Length; i++)
            {
                if (("0123456789" + additionalChars).IndexOf(input[i]) != -1)
                {
                    output.Append(input[i]);
                }
            }
            return output.ToString();
        }

        public static string numbersOnly(this string input)
        {
            return input.numbersOnly("");
        }

        public static string ToMapPath(this string input)
        {
            if(HttpContext.Current != null)
               input =  HttpContext.Current.Server.MapPath(input);
            return input;
        }

		/// <summary>Provides a clean string truncation.</summary>
		/// <param name="Input">Original string</param>
		/// <param name="maxCharacters">Maximum number of characters to keep.</param>
		/// <param name="more">Optional string to append if truncation takes place (like '...').</param>
		/// <param name="wordcut">Maximum number of characters before mid-word cut</param>
		/// <returns>string</returns>
		public static string Truncate(this string Input, int maxCharacters, string more, int wordcut)
		{
			int strEnd = 0;
			if (Input.Length > maxCharacters)
			{
				strEnd = Input.Trim().LastIndexOfAny(new char[] { ' ', '\n', '\r' }, maxCharacters);
				if ((maxCharacters - strEnd) > wordcut) strEnd = maxCharacters - wordcut;
				Input = Input.Substring(0, strEnd).Trim() + more;
			}
			return Input;
		}

		public static string Truncate(this string Input, int maxCharacters)
		{
			return Input.Truncate(maxCharacters, "... ", 12);
		}

		public static string Truncate(this string Input, int maxCharacters, string more)
		{
			return Input.Truncate(maxCharacters, more, 12);
		}

		public static string ProperCase(this string stringInput)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			bool fEmptyBefore = true;
			foreach (char ch in stringInput)
			{
				char chThis = ch;
				if (Char.IsWhiteSpace(chThis))
					fEmptyBefore = true;
				else
				{
					if (Char.IsLetter(chThis) && fEmptyBefore)
						chThis = Char.ToUpper(chThis);
					else
						chThis = Char.ToLower(chThis);
					fEmptyBefore = false;
				}
				sb.Append(chThis);
			}
			return sb.ToString();
		}

        /// <summary>Returns true if valid email</summary>
        /// <returns>bool</returns>
        public static bool isValidEmail(this string s)
        {
            // Less restrictive regex -  (\w+)@(\w+)\.(\w+)
            return (Regex.IsMatch(s, @"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"));
        }

        /// <summary>Returns the character at the position in the string</summary>
        /// <param name="position">Integer position in the string.</param>
        /// <returns>char</returns>
		/// 
        public static char CharAt(this string s, int i)
        {
            return Convert.ToChar(s.Substring(i, 1));
        }
	
        //=======================================================================
        // String Formatting Extensions
        //=======================================================================

        /// <summary>Adds a string to end of the original string if original is not empty.</summary>
        /// <param name="addToEnd">String to add.</param>
        /// <returns>String</returns>
        public static string ifNotEmpty(this string s, string addToEnd)
        {
            s = (s != null) ? s : "";
            return (s.Trim().Length > 0) ? (s += addToEnd) : s;
        }

        /// <summary>Adds strings to the start and end of the original string if original is not empty.</summary>
        /// <param name="addToStart">String to add at the start.</param>
        /// <param name="addToEnd">String to add at the start.</param>
        /// <returns>String</returns>
        public static string ifNotEmpty(this string s, string addToStart, string addToEnd)
        {
            s = (s != null) ? s : "";
            return (s.Trim().Length > 0) ? (addToStart + s + addToEnd) : s;
        }

        //=======================================================================
        // Html Utility Extensions
        //=======================================================================

        /// <summary>Shows xml code in browser.</summary>
        /// <returns>Returns string formatted with HtmlEncode and wrapped in html pre tags.</returns>
        public static string showCode(this string s)
        {
            return s.htmlEncode().ifNotEmpty("<pre>", "</pre>");
        }

		/// <summary>HtmlEncodes an item.</summary>
		/// <returns>String to be formatted.</returns>
		public static string htmlEncode(this string s)
		{
			return System.Web.HttpUtility.HtmlEncode(s);
		}

		/// <summary> HtmlDecodes an item.</summary>
		/// <returns>Decoded string.</returns>
		public static string htmlDecode(this string s)
		{
			return System.Web.HttpUtility.HtmlDecode(s);
		}

        /// <summary>UrlEncodes an item.</summary>
        /// <returns>String to be formatted.</returns>
        public static string urlEncode(this string s)
        {
            return System.Web.HttpUtility.UrlEncode(s);
        }

        /// <summary>UrlDecodes an item.</summary>
        /// <returns>String to be formatted.</returns>
        public static string urlDecode(this string s)
        {
            return System.Web.HttpUtility.UrlDecode(s);
        }

        //=======================================================================
        // String to Numeric Extensions
        //=======================================================================

        /// <summary>Converts strings to Int.</summary>
        /// <returns>An int value</returns>
        public static int toInt(this string value, int defaultValue = 0)
        {
            int result = 0;
            int.TryParse(value, out result);

            return result;
        }

        /// <summary>Converts strings to Int if possible</summary>
        /// <returns>An int value or null</returns>
        public static int? toIntNullable(this string value)
        {
            int result;
            return (int.TryParse(value, out result)) ? (int?) result : null;
        }

        /// <summary>Converts strings to long</summary>
        /// <returns>A long value</returns>
        public static long toLong(this string value, Int64 defaultValue = 0)
        {
            long result = defaultValue;
            long.TryParse(value, out result);

            return result;
        }

        /// <summary>Converts strings to double.</summary>
        /// <returns>An double value</returns>
        public static double toDouble(this string value, double defaultValue = 0D)
        {
            double result = defaultValue;
            double.TryParse(value, out result);
            return result;
        }

        /// <summary>Converts strings to double if possible</summary>
        /// <returns>An double value or null</returns>
        public static double? toDoubleNullable(this string value)
        {
            double result;
            return (double.TryParse(value, out result)) ? (double?) result : null;
        }

		/// <summary> Safely converts strings to Decimal without without having to use an explicit try/catch</summary>
        /// <returns>A Decimal value</returns>
        public static decimal toDecimal(this string value, decimal defaultValue = 0.0M)
        {
            decimal result = defaultValue;
            decimal.TryParse(value, out result);

            return result;
        }

        /// <summary>Converts strings to Decimal if possible</summary>
        /// <returns>An Decimal value or null</returns>
        public static Decimal? toDecimalNullable(this string value)
        {
            Decimal result;
            return (Decimal.TryParse(value, out result)) ? (Decimal?)result : null;
        }

		/// <summary>Converts strings to DateTime with default if unsuccessful</summary>
		/// <returns>A DateTime value</returns>
		public static DateTime toDateTime(this string value, DateTime defaultValue)
		{
			DateTime result = defaultValue;
			DateTime.TryParse(value, out result);

			return result;
		}

		/// <summary>Converts strings to DateTime with default value of nullable (datetime or null) if unsuccessful</summary>
		/// <returns>A nullable DateTime value</returns>
		public static DateTime? toDateTime(this string value, DateTime? defaultValue = null)
		{
			DateTime? result = defaultValue;
			DateTime datetime;
			if (DateTime.TryParse(value, out datetime))
				result = datetime;

			return result;
		}

        /// <summary>Increments integer +1 on the end of a string</summary>
        /// <example>'File.txt'.IncrementString("txt") = 'File1.txt'</example>
        /// <example>'File6.txt'.IncrementString(1,"txt") = 'File1.txt'</example>
        /// <returns>A string with end int incremented +1</returns>
        public static string incrementString(this string str, int? seedIfEmpty =  null, string ignoreEnd = "")
        {
            str = str.removeEnd(ignoreEnd);
            int? currentnumber = seedIfEmpty;
            string numberstring = "";

            numberstring += Regex.Match(str, @"[0-9,-]*$");
            if (numberstring.toIntNullable() != null)
            {
                str = str.Remove(str.Length - numberstring.Length);
                currentnumber = numberstring.toInt() + 1;
            }
            return str + (currentnumber.HasValue ? currentnumber.Value.ToString() : "") + ignoreEnd;
        }

        /// <summary>Replaces line breaks with html line breaks</summary>
        /// <returns>A string with line breaks replaced</returns>
        public static string toHtmlBr(this string str)
        {
            return str.Replace("\r\n", "<br/>").Replace("\r", "<br/>");
        }

    }
}
