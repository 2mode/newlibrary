﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NewLibrary.ForType
{
    public static class Extensions
    {
        public static MetaModel GetMetaModel(this Type type)
        {
            return new MetaModel(type);
        }

        public static MetaModel GetMetaModel<T>(this IEnumerable<T> enumerable)
        {
           Type type = enumerable.GetType().GetGenericArguments()[0]; 
           return new MetaModel(type);
        }

        public static MetaModel GetMetaModel<T>(this T instance)
            where T : class, new()
        {
            if (instance is IEnumerable)
            {
                Type type = instance.GetType().GetGenericArguments()[0];
                return new MetaModel(type);
            }
            return new MetaModel(instance.GetType(), instance);
        }
    }
}
