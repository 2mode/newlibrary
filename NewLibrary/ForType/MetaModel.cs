﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NewLibrary.ForString;
using NewLibrary.ForObject;

namespace NewLibrary.ForType
{
    public class MetaModel
    {
        private Type _type;
        private List<MetaProperty> properties = null;
        private object _instance;
        
        // Constructors
        public MetaModel(Type type, object instance = null)
        {
            _type = type;
            _instance = instance;
        }

        // Public Properties

        public string TypeName {
            get
            {
                if (_type == null)
                    throw new Exception("MetaModel type cannot be null.");

                return _type.Name;
            }
        }

        public string PrimaryKeyName
        {
            get
            {
                return PrimaryKeyMeta.Is() ? PrimaryKeyMeta.Name : "";
            }
        }

        public MetaProperty PrimaryKeyMeta { 
            get {   
                return  properties.FirstOrDefault(a => a.IsKey == true);  
            }      
        }

        public List<MetaProperty> GetMetaProperties(string exclude = null, string include = null)
        {
            if (!exclude.isNullOrEmpty()) 
            {  
                var excludeList = exclude.Split(',').Select(a => a.Trim());
                return  MetaProperties.Where(w => !excludeList.Any(e => w.Name == e)).ToList();
            }

            if(!include.isNullOrEmpty() )
            {
                var includeList = include.Split(',').Select(a => a.Trim());
                return MetaProperties.Where(w => includeList.Any(e => w.Name == e)).ToList(); // returns fields not in list;
            }

            if (!exclude.isNullOrEmpty() && !include.isNullOrEmpty())
            {
                throw new Exception("For the GetMetaProperties method you can only populate the exclude OR the include lists.");
            }

            return MetaProperties.ToList();
        }

        // Private properties
        private List<MetaProperty> MetaProperties
        {
            get
            {
                if (_type == null)
                    throw new Exception("MetaModel type cannot be null.");

                if (properties == null)
                    properties = new List<MetaProperty>();

                if (properties.Count == 0)
                {
                    foreach (var property in _type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                    {

                        properties.Add(new MetaProperty(property, _instance)
                        {
                            Name = property.Name,
                            PropertyType = property.PropertyType
                            // OLD CODE - now returns Nullable<T> : PropertyType = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType,
                        });
                    }
                }
                return properties;
            }
        }
    }
}
