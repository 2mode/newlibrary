﻿using System;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace NewLibrary.ForType
{
    public class MetaProperty
    {
        // Private Properties
        private PropertyInfo propertyInfo;
        private object _instance;

        // Constructor
        public MetaProperty(PropertyInfo propertyinfo)
        {
            propertyInfo = propertyinfo;
        }

        public MetaProperty(PropertyInfo propertyinfo, object instance)
        {
            propertyInfo = propertyinfo;
            _instance = instance;
        }

        // Public Properties
        public string Name { get; set; } 
        public Type PropertyType { get; set; }
        public bool IsKey { 
            get {  return propertyInfo.IsDefined(typeof(KeyAttribute), false); }
        }
        public bool CanWrite
        {
            get { return propertyInfo.CanWrite; }
        }

        // Public Methods
        public dynamic GetInstanceValue(object instance = null)
        {
            if (instance != null)
            {
                _instance = instance;
            }
            if (_instance == null)
            {
                throw new Exception("This method requires a non-null instance in the MetaType or MetaProperty constructor or as an argument.");
            }

            // ALT VERSION
            //dynamic obj = propertyInfo.GetValue(_instance, null);
            //if(obj != null && obj is IConvertible)
            //{
            //    return Convert.ChangeType(obj, PropertyType);
            //} else { 
            //    return obj;
            //}

            return propertyInfo.GetValue(_instance, null);
        }

        public void SetInstanceValue(object value, object instance = null)
        {
            if (instance != null)
            {
                _instance = instance;
            }
            if (_instance == null)
            {
                throw new Exception("This method requires a non-null instance in the MetaType or MetaProperty constructor or as an argument.");
            }

            propertyInfo.SetValue(_instance, value, null);
        }
    }
}
