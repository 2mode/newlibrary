﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace NewLibrary.ForIQueryable
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> items, string propertyName, 
                                               bool desc = false, string defaultProperty = "")
        {
            try
            {
                var typeOfT = typeof(T);
                var parameter = Expression.Parameter(typeOfT, "parameter");
                var property = (!String.IsNullOrWhiteSpace(propertyName)) ? typeOfT.GetProperty(propertyName) : typeOfT.GetProperty(defaultProperty);
                if (property == null) // Invalid propertyName - use first property
                {
                    property = typeOfT.GetProperties()[0];
                }
                var propertyAccess = Expression.PropertyOrField(parameter, property.Name);
                var orderExpression = Expression.Lambda(propertyAccess, parameter);
                string orderString = (desc == false) ? "OrderBy" : "OrderByDescending";

                var expression = Expression.Call(typeof(Queryable), orderString,
                    new Type[] { typeOfT, property.PropertyType }, items.Expression, Expression.Quote(orderExpression));
            
                return items.Provider.CreateQuery<T>(expression);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
} 

 
