﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace NewLibrary.ForObject
{
    public static class ObjectExtensions
    {
        public static string ToString(this Object arg0, string replace = null, string format = null)
        {
            if (arg0.Is()) {
                return format.Is() ? String.Format(format, arg0) : "";
            } else  {
                return replace.Is() ? replace : "";
            }
        }
        
        public static bool Is(this Object arg0)
        {
            return (arg0 != null);
        }

        public static bool IsNull(this Object arg0)
        {
            return (arg0 == null);
        }
    }
} 

 
