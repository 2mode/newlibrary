﻿/* © Copyright Will Crowther 2014 under the GNU General Public License (GPLv3) */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Reflection;
using NewLibrary.ForType;

namespace NewLibrary.ForIEnumerable
{
    public static class IEnumerableExtensions
    {

        /* EXAMPLE of Next
            var contacts = new List<Contact> {
                new Contact { Id=1,  Name = "Chris Smith"},
                new Contact { Id=2,  Name = "Bob Harris" },
                new Contact { Id=3,  Name = "Fred" },
                new Contact { Id=4,  Name = "Jane"}
            };
            var next = contacts.Next<Contact, Int>(2, (a, b) => a.Id == b);
            // returns Contact(3, "Fred")
        */

        /// <summary>Given an IEnumerable with a unique property will return next record. Otherwise returns default for type.</summary>
        public static T1 Next<T1, T2>(this IEnumerable<T1> list, T2 property, Func<T1, T2, bool> fn)
        {
            var enumerator = list.GetEnumerator();
            bool isCurrent = false;
            T1 next =  default(T1);
            while (enumerator.MoveNext())
            {
                // Skip unless isCurrent is true from last loop
                if (isCurrent)
                {
                    next = enumerator.Current;
                    break;
                }
                isCurrent = fn(enumerator.Current, property);
            }
            return next;
        }

        /// <summary>Given an IEnumerable with a unique property will return previous record. Otherwise returns default for type.</summary>
        public static T1 Previous<T1, T2>(this IEnumerable<T1> list, T2 property, Func<T1, T2, bool> fn)
        {
            var enumerator = list.GetEnumerator();
            T1 previous = default(T1);
            int loop = 0;
            while (enumerator.MoveNext())
            {
                // If isCurrent then return item from last loop
                if (fn(enumerator.Current, property))
                {
                    break;
                }
                previous = enumerator.Current;
                loop++;
            }
            // If only one element is list cannot have a previous record

            return (loop > 0) ? previous : default(T1);
        }

        /// <summary>Will return a sequence of type T while function Func returns true</summary>
        public static IEnumerable<T> Sequence<T>(this IEnumerable<T> sequence, Func<T, bool> func)
        {
            var ie1 = sequence.GetEnumerator();
            while (ie1.MoveNext() && func(ie1.Current))
            {
                yield return ie1.Current;
            }
        }

        /// <summary>Will return a sequence of type T while function Func returns true passing in an int iterator. 'Row' determines initial value of int.</summary>
        public static IEnumerable<T> Sequence<T>(this IEnumerable<T> sequence, Func<T, int, bool> func, int seed = 1)
        {
            var ie1 = sequence.GetEnumerator();
            int i = seed;
            while (ie1.MoveNext() && func(ie1.Current, i))
            {
                i++;
                yield return ie1.Current;
            }
        }

        /// <summary>Given two lists returns to values from first if func is true. 
        /// If consecutive is false continues until one of the lists has no  more elements.</summary>
        public static IEnumerable<TFirst> Sequence<TFirst, TSecond>(this IEnumerable<TFirst> first,
                                                                         IEnumerable<TSecond> second, Func<TFirst, TSecond, bool> func,
                                                                         bool consecutive = true)
        {
            var ie1 = first.GetEnumerator();
            var ie2 = second.GetEnumerator();
            bool match = true; // initialize as true
            bool next = true;

            while (ie1.MoveNext() && next )
            {
                if (consecutive || match)
                {
                    next = ie2.MoveNext();
                }
                if (next && func(ie1.Current, ie2.Current))
                {
                    match = true;
                    yield return ie1.Current;
                }
                else
                {
                    match = false;
                }
            }
        }

        public static object GetDefaultValue(Type t)
        {
            if (t.IsValueType)
                return Activator.CreateInstance(t);

            return null;
        }
        
        /// <summary>Converts IEnumerable to Collection of type paramter.
        /// Returns an empty Collection rather than failing if enumerable is null.</summary>
        /// <![CDATA[Example: return tiers.OfType<OrderItem>().ToCollection<OrderItem>();</example>]]]>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns>Returns Collection of type.</returns>
        public static Collection<T> ToCollection<T>(this IEnumerable<T> enumerable)
        {
            var collection = new Collection<T>();

            if (enumerable == null)
                enumerable = new Collection<T>();

            foreach (T i in enumerable)
            {
                collection.Add(i);
            }
            return collection;
        }

        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (var item in collection)
            {
                action(item);
            }
        }
    }
} 

 
