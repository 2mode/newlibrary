﻿using System;
using System.Collections.Generic;

namespace SeedPacket
{
    public interface ISeedRules 
    {
        SeedRule GetRuleByTypeAndName(Type ruleType, string propertyName);
        void Add(SeedRule seedRule);
        void RemoveRuleByName(string ruleName);
        void Clear();
        int Row { get; set; }
        DateTime BaseDateTime { get; }
        Random BaseRandom { get; }
        Dictionary<string, object> CurrentSeedValues { get;}
    }
} 

 
