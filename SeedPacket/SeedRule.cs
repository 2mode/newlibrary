﻿using System;
using System.ComponentModel.DataAnnotations;
using NewLibrary.ForType;
using NewLibrary.ForString;

namespace SeedPacket
{
    public class SeedRule 
    {
        #region Private Fields

        private string ruleName;
        private Type typeMatch;
        private string nameMatch;
        private Func<ISeedRules, dynamic, dynamic> rule; 

        #endregion

        #region Constructor

        public SeedRule(string ruleName, Type typeMatch, string nameMatch, Func<ISeedRules, dynamic, dynamic> rule, string description = "")
        {
            this.ruleName = ruleName;
            this.typeMatch = typeMatch;
            this.nameMatch = nameMatch.isBlank().ToLower();
            this.rule = rule;
            this.Description = description;
        } 

        #endregion

        [StringLength (30)] 
        public string RuleName {
            get{ return ruleName;}
            private set { ruleName = value; }
        }

        public string Description { get; set; }

        public bool IsMatch(Type propType, string propName)
        {
            // Must match on type, if not the same then false - no match for this rule
            // ('typeMatch' is the type this rule applies to)

            if (propType.IsAssignableFrom(typeMatch))
            {               
                // Will except comma-separated list strings for match.
                return NameMatches(nameMatch, propName.isBlank().ToLower());
            }
            return false;
        }

        public dynamic ApplyRule(ISeedRules config, MetaProperty property)
        {
            return rule(config, property);
        }

        #region Private Methods

        private static bool NameMatches(string namematch, string propname)
        {
            // If comma in string, break into individual strings and loop through each
            if (namematch.Contains(","))
            {
                var nameArray = namematch.Split(new[]{','}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var name in nameArray)
                {
                    if (NameMatch(name, propname))
                    {
                        return true;
                    }
                }
                return false;
            }

            return NameMatch(namematch, propname);
        } 

        private static bool NameMatch(string namematch, string propname)
        {
            // 1. type matches but propname has not been defined for rule then true
            // 2. type matches and propname has wildcard. if wildcard matches then true otherwise false
            // 3. type matches but propName does not match one defined for this rule then false

            if (namematch.isNullOrEmpty())
            {
                return true;
            }
            else if (namematch.StartsWith("%") && namematch.EndsWith("%"))
            {
                return propname.Contains(namematch.TrimStart('%').TrimEnd('%'));
            }
            else if (namematch.StartsWith("%"))
            {
                return propname.EndsWith(namematch.TrimStart('%'));
            }
            else if (namematch.EndsWith("%"))
            {
                return propname.StartsWith(namematch.TrimEnd('%'));
            }
            else if (namematch == propname)
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
        #endregion
    }
} 

 
