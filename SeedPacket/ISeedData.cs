﻿using System.Collections.Generic;

namespace SeedPacket
{
    public interface ISeedData
    {
        List<SeedRule> CreateAdvancedRules();
        List<SeedRule> CreateBasicRules();
    }
}
