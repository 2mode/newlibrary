﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NewLibrary.ForString;
using NewLibrary.ForObject;

namespace SeedPacket
{
    public class SeedRules : Collection<SeedRule>, ISeedRules
    {
        
    #region Constructor

        public SeedRules(DateTime? baseDateTime = null,
                    Random baseRandom = null,
                    SeedRulesSet baseRuleSet = SeedRulesSet.Advanced,
                    ISeedData seedData = null,
                    IList<SeedRule> rules = null)
        {
            this.baseRandom = baseRandom;
            this.baseDateTime = baseDateTime;
            this.BaseRulesSet = baseRuleSet;
            this.seedData = seedData ?? new SeedData() as ISeedData;

            CreateDefaultRules();

            if (rules.Is())
            {
                this.AddRange(rules);
            }
        }  
    #endregion

    #region Private Fields

        private Random baseRandom;
        private DateTime? baseDateTime;
        private ISeedData seedData;
        private Dictionary<string, object> currentSeedValues = new Dictionary<string, object>();

    #endregion

        public Random BaseRandom
        {
            get {
                if (baseRandom == null)
                    baseRandom = new Random(1235);

                return baseRandom;
            }
            private set {baseRandom = value; }
        }

        public DateTime BaseDateTime
        {
            get { return baseDateTime ?? DateTime.Now; }
            private set { baseDateTime = value; }
        }

        public int Row { get; set; }

        public SeedRulesSet BaseRulesSet { get; set; }

        public SeedRule GetRuleByTypeAndName(Type propertyType, string propertyName)
        {
            // Rules are process last to first

            SeedRule rule = null;
            int i = this.Count - 1; // zero based
            bool matchfound = false;

            while (i >= 0 && !matchfound)
            {
                if (this[i].IsMatch(propertyType, propertyName))
                {
                    matchfound = true;
                    rule = this[i];
                }
                i--;
            }
            return rule;
        }

        // Temp dictionary of values in current seed
        public Dictionary<string, object> CurrentSeedValues { 
            get { return currentSeedValues ; }
        }

        public void Add(SeedRule seedRule, bool overwrite = true)
        {
            if (seedRule.RuleName.isNullOrEmpty() && seedRule.RuleName.Length <= 3)
                throw new Exception("SeedRule RuleName must be at least 3 characters in length.");

            if (this.Any(a => a.RuleName == seedRule.RuleName))
            {
                if (overwrite)
                {
                    RemoveRuleByName(seedRule.RuleName);
                }
                else
                {
                    throw new Exception("SeedRule RuleName already exists.");
                }
            }
            base.Add(seedRule);
        }

        public void AddRange(IEnumerable<SeedRule> seedRules, bool overwrite = true)
        {
            foreach(var seedRule in seedRules){
                this.Add(seedRule, overwrite);
            }
        }

        public void RemoveRuleByName(string ruleName) {
           var rule = this.Where(a => a.RuleName == ruleName).FirstOrDefault();
           if(rule != null){
                base.Remove(rule);
           }
        }

    #region Private Create Rules 

        private void CreateDefaultRules()
        {
            if (BaseRulesSet != SeedRulesSet.None)
            {
                this.AddRange(seedData.CreateBasicRules());
            }

            if (BaseRulesSet == SeedRulesSet.Advanced)
            {
                this.AddRange(seedData.CreateAdvancedRules());
            }
        }

    #endregion

    }
} 

 
