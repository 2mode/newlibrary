﻿using System;
using System.Collections.Generic;
using NewLibrary.ForType;
using NewLibrary.ForString;
using NewLibrary.ForObject;
using System.Diagnostics;

namespace SeedPacket
{
    public static class SeedExtensions
    {
        public static IEnumerable<T> Seed<T>(this IEnumerable<T> ienumerable, int count) where T : new()
        {
            return Seed<T>(ienumerable, 1, count);
        }

        public static IEnumerable<T> Seed<T>(this IEnumerable<T> ienumerable, 
                                             int seedBegin = 1, int seedEnd = 10,
                                             ISeedRules seedRules = null) where T : new()
        {
            ISeedRules rules = seedRules ?? new SeedRules();
            var cachedRules = new Dictionary<int,SeedRule>();
            var metaProperties = typeof(T).GetMetaModel().GetMetaProperties();
            var seedList = new List<T>();
            bool firstRow = true;

            Debug.WriteLine("Begin Seed Creation for Type: " + typeof(T).Name);

            for (int i = seedBegin; i <= seedEnd; i++)
            {
                CreateSeed(seedList, metaProperties, firstRow, rules, cachedRules, i);
                firstRow = false;
            }
            ienumerable = seedList;
            return ienumerable;
        }

        private static void CreateSeed<T>( List<T> seedlist, List<MetaProperty> metaProperties, bool firstRow, ISeedRules rules,
                                           Dictionary<int, SeedRule> cachedRules, int i) where T : new()
        {
            // Create each seed item
            dynamic newItem = Activator.CreateInstance(typeof (T)); // new T(); // 

            for (int p = 0; p <= metaProperties.Count - 1; p++)
            {
                SetSeedPropertyValue<T>( newItem, metaProperties, p, firstRow, rules, cachedRules, i);
            }
            seedlist.Add(newItem);

            // Temp dictionary of other values in this seed. Cleared after loop.
            rules.CurrentSeedValues.Clear(); 
        }

        private static void SetSeedPropertyValue<T>( dynamic newItem, List<MetaProperty> metaProperties, int p, bool firstRow,
                                                     ISeedRules rules, Dictionary<int, SeedRule> cachedRules, int i) where T : new()
        {
            SeedRule rule = null;
            MetaProperty property = metaProperties[p];

            if (!property.CanWrite)
                return;

            if (firstRow) // Cache rules for first row
            {
                rule = rules.GetRuleByTypeAndName(property.PropertyType, property.Name);
                cachedRules.Add(p, rule);
                Debug.WriteLine(String.Format("First Seed Row PropertyValue for {0}({1}) using rule: {2}.", property.Name,
                    property.PropertyType, (rule.Is() ? rule.RuleName.isBlank() : "None (using default)")));
            }
            else
            {
                rule = cachedRules[p];
                // Debug.WriteLine("using cachedRule " + rule.RuleName);
            }

            if (rule.Is())
            {
                rules.Row = i;
                dynamic seedValue = rule.ApplyRule(rules, property);
                property.SetInstanceValue(seedValue, newItem);
                rules.CurrentSeedValues.Add(property.Name, seedValue);
            }
        }

        // TO DO DICTIONARY

        //public static Dictionary<int, object> ExampleDictionary(ISeedRules rules, MetaProperty property)
        //{
        //    int diceroll = rules.BaseRandom.Next(0, 5);
        //    var d = new Dictionary<int, object>();

        //    return new Dictionary<int, object>().Seed(diceroll).ToDictionary<int, object>()
        //}
    }
} 

 
