﻿using System;
using System.Collections.Generic;
using System.Linq;
using NewLibrary.ForType;
using System.Globalization;

namespace SeedPacket
{
    public class SeedData : ISeedData 
    {
        public List<SeedRule> CreateBasicRules()
        {
             return new List<SeedRule>(){
                    new SeedRule("Basic string",    typeof(String),     "", (rules, prop) => prop.Name + rules.Row.ToString(CultureInfo.CurrentCulture),  "Returns propertyName + Row." ),
                    new SeedRule("Basic bool",      typeof(bool),       "", (rules, prop) => (rules.Row % 2 == 0) ? true : false,  "Alternating true & false."),
                    new SeedRule("Basic int",       typeof(int),        "", (rules, prop) => rules.Row, "Returns Row"),
                    new SeedRule("Basic long",      typeof(long),       "", (rules, prop) => (long) rules.Row, "Returns Row" ),
                    new SeedRule("Basic double",    typeof(double),     "", (rules, prop) => (double) rules.Row, "Returns Row" ),
                    new SeedRule("Basic decimal",   typeof(decimal),    "", (rules, prop) => (decimal) rules.Row, "Returns Row" ),
                    new SeedRule("Basic DateTime",  typeof(DateTime),   "", (rules, prop) => rules.BaseDateTime, "Uses BaseDateTime" ),

                    new SeedRule("Basic bool?",     typeof(bool?),      "", (rules, prop) => (rules.BaseRandom.Next(1, 6) == 5) ? (bool?) null      : (rules.Row % 2 == 0), "Returns alternating true & false (1 in 5 NULL)"),                                 
                    new SeedRule("Basic int?",      typeof(int?),       "", (rules, prop) => (rules.BaseRandom.Next(1, 6) == 5) ? (int?) null       : (int?) rules.Row,     "Returns Row (1 in 5 NULL)"),                                 
                    new SeedRule("Basic long?",     typeof(long?),      "", (rules, prop) => (rules.BaseRandom.Next(1, 6) == 5) ? (long?) null      : (long?) rules.Row, "Returns Row (1 in 5 NULL)"),
                    new SeedRule("Basic double?",   typeof(double?),    "", (rules, prop) => (rules.BaseRandom.Next(1, 6) == 5) ? (double?) null    : (double?) rules.Row, "Returns Row (1 in 5 NULL)"),
                    new SeedRule("Basic decimal?",  typeof(Decimal?),   "", (rules, prop) => (rules.BaseRandom.Next(1, 6) == 5) ? (decimal?) null   : (decimal?) rules.Row, "Returns Row (1 in 5 NULL)"),                                 
                    new SeedRule("Basic DateTime?", typeof(DateTime?),  "", (rules, prop) => (rules.BaseRandom.Next(1, 6) == 5) ? (DateTime?) null  : rules.BaseDateTime,   "Returns BaseDateTime (1 in 5 NULL)")                               
                };
        }

        public List<SeedRule> CreateAdvancedRules()
        {
            return new List<SeedRule>(){
                    new SeedRule("Advanced FirstName",  typeof(String),"%firstname%",       (rules, prop) => RandomFirstName(rules, prop), "Random FirstName" ),
                    new SeedRule("Advanced LastName",   typeof(String),"%lastname%",        (rules, prop) => RandomLastName(rules, prop), "Random lastname" ),
                    new SeedRule("Advanced User",       typeof(String),"%user%",            (rules, prop) => RandomUserName(rules, prop), "Random username" ),
                    new SeedRule("Advanced Email",      typeof(String),"%email%",           (rules, prop) => RandomEmail(rules, prop), "Random email" ),
                    new SeedRule("Advanced Address",    typeof(String),"%address%",         (rules, prop) => RandomAddress(rules, prop), "Random address" ),
                    new SeedRule("Advanced City",       typeof(String),"%county%",          (rules, prop) => RandomCity(rules, prop), "Random city" ),
                    new SeedRule("Advanced County",     typeof(String),"%country%",         (rules, prop) => RandomCounty(rules, prop), "Random country" ),
                    new SeedRule("Advanced State",      typeof(String),"%state%",           (rules, prop) => RandomState(rules, prop), "Random state" ),
                    new SeedRule("Advanced Country",    typeof(String),"%country%",         (rules, prop) => RandomCountry(rules, prop), "Random country" ),
                    new SeedRule("Advanced Zip",        typeof(String),"%zip%",             (rules, prop) => RandomZip(rules, prop), "Random zip" ),
                    new SeedRule("Advanced Fee/Tax",                typeof(String),"%fee%,%tax%",                   (rules, prop) => RandomFee(rules, prop), "Random fee/tax" ),
                    new SeedRule("Advanced Cost/Price",             typeof(String),"%cost%,%price%",                (rules, prop) => RandomCost(rules, prop), "Random cost/price" ),
                    new SeedRule("Advanced Company/Business",       typeof(String),"%company%,%business%",          (rules, prop) => RandomCompany(rules, prop), "Random company/business" ),
                    new SeedRule("Advanced Product/Item",           typeof(String),"%product%,%item%",              (rules, prop) => RandomProduct(rules, prop), "Random product/item" ),
                    new SeedRule("Advanced Phone/Cell/Mobile/Fax",  typeof(String),"%phone%,%cell%,%mobile%,%fax%",(rules, prop) => RandomPhone(rules, prop), "Random phone/cell/mobile/fax" )
                };
        }

    #region Random Generate Methods

        public string RandomFirstName(ISeedRules rules, MetaProperty property)
        {
            int index = rules.BaseRandom.Next(FirstNames.Length);
            return FirstNames[index];
        }

        public string RandomLastName(ISeedRules rules, MetaProperty property)
        {
            int index = rules.BaseRandom.Next(LastNames.Length);
            return LastNames[index];
        }

        public string RandomUserName(ISeedRules rules, MetaProperty property)
        {
            int i1 = rules.BaseRandom.Next(FirstNames.Length);
            int i2 = rules.BaseRandom.Next(LastNames.Length);

            return FirstNames[i1].First() + LastNames[i2]; // First-Initial LastName
        }

        public string RandomEmail(ISeedRules rules, MetaProperty property)
        {
            int i1 = rules.BaseRandom.Next(FirstNames.Length);
            int i2 = rules.BaseRandom.Next(LastNames.Length);
            int i3 = rules.BaseRandom.Next(CompanyNames.Length);
            int i4 = rules.BaseRandom.Next(CompanySuffixes.Length);
            int i5 = rules.BaseRandom.Next(DomainExtensions.Length);
            string email = FirstNames[i1].First() + LastNames[i2] + "@" + CompanyNames[i3] + CompanySuffixes[i4] + DomainExtensions[i5];

            return email.ToLower();
        }

        public string RandomAddress(ISeedRules rules, MetaProperty property)
        {
            int i1 = rules.BaseRandom.Next(1,10000);
            int i2 = rules.BaseRandom.Next(StreetNames.Length);
            int i3 = rules.BaseRandom.Next(RoadTypes.Length);

            return String.Format("{1} {2} {3}", i1, StreetNames[i2], RoadTypes[i3]);
        }

        public string RandomCity(ISeedRules rules, MetaProperty property)
        {
            int index = rules.BaseRandom.Next(CityNames.Length);
            return CityNames[index];
        }

        public string RandomCounty(ISeedRules rules, MetaProperty property)
        {
            int index = rules.BaseRandom.Next(CountyNames.Length);
            return CountyNames[index];
        }

        public string RandomState(ISeedRules rules, MetaProperty property)
        {
            int index = rules.BaseRandom.Next(LastNames.Length);
            return LastNames[index];
        }

        public string RandomCountry(ISeedRules rules, MetaProperty property)
        {
            int index = rules.BaseRandom.Next(LastNames.Length);
            return LastNames[index];
        }

        public string RandomZip(ISeedRules rules, MetaProperty property)
        {
            int i1 = rules.BaseRandom.Next(10001,100000);
            return i1.ToString();
        }

        public decimal RandomFee(ISeedRules rules, MetaProperty property)
        {
            decimal fee = rules.BaseRandom.Next(1, 200);
            decimal dec = rules.BaseRandom.Next(0, 100);
            return fee + (dec * .01M);
        }

        public decimal RandomCost(ISeedRules rules, MetaProperty property)
        {
            decimal cost = rules.BaseRandom.Next(1, 1000);
            decimal dec = rules.BaseRandom.Next(0, 100);
            return cost + (dec * .01M);
        }

        public string RandomCompany(ISeedRules rules, MetaProperty property)
        {
            int i1 = rules.BaseRandom.Next(CompanyNames.Length);
            int i2 = rules.BaseRandom.Next(CompanySuffixes.Length);

            return (CompanyNames[i1] + " " + CompanySuffixes[i2]).Trim();
        }

        public string RandomProduct(ISeedRules rules, MetaProperty property)
        {
            int index = rules.BaseRandom.Next(ProductNames.Length);
            return ProductNames[index];
        }

        public string RandomPhone(ISeedRules rules, MetaProperty property)
        {
            int i1 = rules.BaseRandom.Next(100,1000);
            int i2 = rules.BaseRandom.Next(100,1000);
            int i3 = rules.BaseRandom.Next(1000,10000);

            return String.Format("({0}) {1}-{2}", i1, i2, i3);
        }

        public int RandomId(ISeedRules rules, MetaProperty property)
        {
            int index = rules.BaseRandom.Next(Ids.Length);
            return Ids[index];
        }

    #endregion

    #region Seed Data Lists

        public string[] FirstNames = new[] {   "John", "Patricia", "Michael", "Susan", "William", "Mary", "Robert", "Sarah", "Peter", "Ann",
                                                "Roger", "Theresa", "Ethan", "Crystal", "Phillip", "Denise", "Juan", "Amber", "Randy", "Tiffany",
                                                "Alan", "Janice", "Christian", "Gloria", "Jesse", "Mildred", "Patrick", "Christine", "Gary", "Rebecca",
                                                "Dylan", "Jacqueline", "Treymayne", "Nicole", "Nicholas", "Karen", "Christopher", "Jennifer", "Levi", "Courtney",
                                                "Xavier", "Theresa", "Omar", "Crystal", "Jorge", "Summer", "Cesar", "Naomi", "Travis", "Zoey"
        };

        public string[] LastNames = new[] {   "Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor",
                                               "Anderson", "Thomas", "Jackson", "White", "Harris", "Martin", "Thompson", "Garcia", "Martinez", "Robinson",
                                               "Clark", "Rodriguez", "Lewis", "Lee", "Walker", "Hall", "Allen", "Young", "Hernandez", "King",
                                               "Wright", "Lopez", "Hill", "Scott", "Green", "Adams", "Baker", "Gonzalez", "Nelson", "Carter",
                                               "Mitchell", "Perez", "Roberts", "Turner", "Phillips", "Campbell", "Parker", "Evans", "Edwards", "Collins"
        };

        public string[] CompanyNames = new[] { "Acme", "Globax", "Virtucon", "Wonka", "Stark", "Spacely", "Tyrell", "Oscorp", "More", "Nakatomi",
                                                "Umbrella", "Compudata", "Jackson", "Wayne", "Parallax", "Mega", "CyberGlobe", "Primo", "Holdings", "Capital",
                                                "Next", "Sunrise", "MultiNational", "United", "City"
        };

        public string[] CompanySuffixes = new[] { "", "", "", "", "", "Co", "Co", "Co", "Co", "Corp", "Corp", "LLC", "Industries", "Corporation", "Ind", "Systems", "Inc", "Inc" };

        public string[] DomainExtensions = new[] { ".com", ".com", ".com", ".com", ".com", ".com", ".com", ".net", ".us", ".biz", ".gov" };

        public string[] StreetNames = new[] {  "Main", "South", "West", "East", "North", "Poplar", "Pine", "LakeShore", "Ivy", "Rose",
                                                "Desert", "Valley", "HillTop", "Creek", "Oasis", "Green", "Oakside", "River", "Fielding", "DeadEnd",
                                                "Capital", "Sunrise", "Lakes", "City", "Broad"
        };
        public string[] RoadTypes = new[] {     "Way", "St.", "Trl.", "Blvd", "Street", "Road", "Rd.", "Lane", "Circle", "Ave.", "Avenue" };

        public string[] CityNames = new[] {    "Greenville", "Washington", "Lincoln", "Lincolnton", "Pinetown", "Athens", "York", "London", "Peachtree", "Brighton",
                                                "HillTown", "Port Jackson", "Jackson", "Austin", "Metropolis", "Gotham", "Sierra", "WestLake", "Stone Bridge", "Oakleaf",
                                                "Franklin", "Kingston", "Eastdale", "Winterspring", "Westmoor"
        };

        public string[] CountyNames = new[] {  "Baker", "Baldwin", "Banks", "Camden", "Chandler", "Clarke", "Dade", "Douglas", "Franklin", "Jackson",
                                                "Madison", "Mitchell", "Pierce", "Stewart", "Turner", "Towns", "Warren", "Webster", "White", "Worth"
        };

        public string[] StateNames = new[] {   "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA",  "KS", "KY", "LA", "ME", "MD", "MA",
                                                "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK",  "OR", "PA", "RI", "SC", "SD", "TN",
                                                "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"
        };

        public string[] ProductNames = new[] { "dooHickey", "gadget", "widget", "thingamajig", "gizmo", "whatchamacallit", "doodad", "gimmick", "thingamabob", "whatnot" };

        public int[] Ids = new[] { 342, 6477, 4569, 32556, 4046, 8088, 228, 6679, 121, 245644, 904, 39996, 23660, 790, 45646, 2004, 1006, 766532, 333, 23109,
                                   122, 3, 4909, 1259, 6902, 87688, 33443, 23850, 4400, 40920, 5563, 44522, 722, 234, 8301, 2252, 14, 2354, 75, 9888 };
    
    #endregion

    }
} 

 
