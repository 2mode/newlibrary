﻿using System.Collections.Generic;
using NewLibrary.ForType;

namespace SeedPacket
{
    public static class SeedExampleRules
    {
        // Demo Extension Methods
        public static object AlternatingFormat(ISeedRules rules, MetaProperty property)
        {
            string seed = rules.Row.ToString();
            string format1 = property.Name + "/" + seed;
            string format2 = property.Name + "_" + seed;

            return (rules.Row % 2 == 0) ? format1 : format2;
        }

        public static string RandomItem(ISeedRules rules, MetaProperty property)
        {
            var names = new[] { "dooHickey", "gadget", "widget", "thingamajig", "gizmo", "whatchamacallit", "doodad", "gimmick", "thingamabob", "whatnot" };
            int index = rules.BaseRandom.Next(names.Length); 

            return names[index];
        }

        public static decimal? OccasionalNull(ISeedRules rules, MetaProperty property)
        {
            int diceroll = rules.BaseRandom.Next(1, 7);  // 1 to 6
            return (diceroll == 6) ? null : (decimal?)rules.Row;
        }

        public static decimal? NullableDecimal(ISeedRules rules, MetaProperty property)
        {
            int diceroll = rules.BaseRandom.Next(1, 7);
            return (diceroll == 6) ? null : (decimal?)rules.Row;
        
        }

        public static IEnumerable<T> AddItems<T>(ISeedRules rules, MetaProperty property) where T : new()
        {
            int itemsToAdd = rules.BaseRandom.Next(0, 6); // 0 to 5 items
            return new List<T>().Seed(1, itemsToAdd);
        }
    }
} 

 
